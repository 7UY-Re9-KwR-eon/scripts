#!/bin/zsh

# scripts fetches latest deployment date for all the deployments in the current k8s context
# ex1. filter on specific app: `./deployment-dates.sh r2`
# ex2. fetch app the apps deployed on a given date: `./deployment-dates.sh | grep '2021-12-16'`
# ex3. sorted output: `./deployment-dates.sh r2 | grep '2021-12-16' | sort`

PATTERN=${1:-""}

fetch_deployment_status() {
  app=$1
  updated=$(kubectl rollout history deployment $app -o json | jq '.status.conditions[0].lastUpdateTime')
  printf "{\"lastUpdateTime\": %20s, \"deployment\": %s}\n" $updated $app
}

for app in $(kubectl get deployment --no-headers | awk '{print $1}' | grep "$PATTERN"); do
  fetch_deployment_status $app &
done
wait
